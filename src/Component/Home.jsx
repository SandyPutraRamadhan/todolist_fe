import React from "react";
import "../Style/Home.css";
import Navbar from "./Navbar";
import { MDBIcon } from "mdb-react-ui-kit";
import Footer from "./Footer";

export default function Home() {
  return (
    <div>
      <Navbar />
      <div class="container mt-100 mt-[80px]">
        <div class="row" />
        <div class="col-12 text-center">
          <div class="section-title ">
            <h4 class="title">
              <span className="bg-gradient-to-r from-green-300 via-blue-500 to-purple-600 bg-clip-text text-3xl font-extrabold text-transparent sm:text-6xl">
                Presensi &{" "}
              </span>{" "}
              <span>
                <p className="h1 text-center mt-3 mb-4 pb-3 text-success">
                  <MDBIcon fas icon="check-square" className="me-1" />
                  <u>Todo-List</u>
                </p>
              </span>
            </h4>
            <h4 class="text-muted para-desc mx-auto mb-0">
              {" "}
              aplikasi to do list ini berguna untuk mengelola tugas, aktivitas,
              dan pengingat harian
            </h4>
          </div>
        </div>

       

        <div class="row flex ml-[100px] justify-center gap-60 ">

        <div class="col-lg-4 col-md-6 mt-4 pt-2 shadow-[0_35px_60px_-15px_rgba(0,0,0,0.3)]">
            <div class="blog-post rounded-[30px] ">
              <div class="blog-img d-block overflow-hidden position-relative">
                <img
                  src="https://presensi.bkd.jatimprov.go.id/login_files/img/bg-finger.png"
                  class="img-fluid rounded-top"
                  alt=""
                />
                <div class="overlay rounded-top bg-dark"></div>
                <div class="post-meta">
                  <a
                    href="javascript:void(0)"
                    class="text-light d-block text-right like"
                  >
                    <i class="mdi mdi-heart"></i> 33
                  </a>
                  <a href="/presensi" class="text-light read-more">
                    Read More <i class="mdi mdi-chevron-right"></i>
                  </a>
                </div>
              </div>
              <div class="content p-3">
                <small>
                  <a href="javascript:void(0)" class="text-primary">
                    Software
                  </a>
                </small>
                <h4 class="mt-2">
                  <a href="javascript:void(0)" class="text-dark title">
                    Presensi
                  </a>
                </h4>
                <p class="text-muted mt-2">
                presensi adalah sistem yang digunakan untuk menunjukkan kehadiran seseorang. Presensi biasanya diimplementasikan dalam berbagai skenario, mulai dari event, sekolah, hingga perusahaan.
                </p>
                <div class="pt-3 mt-3 border-top d-flex">
                  <img
                    src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzDGsu13rv5VYmxD817BMb94W-r-FYbcrYfA&usqp=CAU"
                    class="img-fluid avatar avatar-ex-sm rounded-pill mr-3 shadow"
                    alt=""
                  />
                  <div class="author mt-2">
                    <h6 class="mb-0">
                      <a href="/presensi" class="text-dark name">
                      Let's start!
                      </a>
                    </h6>
                  </div>
                </div>
              </div>
            </div>
          </div>



          <div class="col-lg-4 col-md-6 mt-4 pt-2 shadow-[0_35px_60px_-15px_rgba(0,0,0,0.3)]">
            <div class="blog-post rounded-[30px] ">
              <div class="blog-img d-block overflow-hidden position-relative">
                <img
                  src="https://cdn.dribbble.com/users/5325964/screenshots/11432898/todo-logo1-01_4x.png"
                  class="img-fluid rounded-top"
                  alt=""
                />
                <div class="overlay rounded-top bg-dark"></div>
                <div class="post-meta">
                  <a
                    href="javascript:void(0)"
                    class="text-light d-block text-right like"
                  >
                    <i class="mdi mdi-heart"></i> 33
                  </a>
                  <a href="/list" class="text-light read-more">
                    Read More <i class="mdi mdi-chevron-right"></i>
                  </a>
                </div>
              </div>
              <div class="content p-3">
                <small>
                  <a href="javascript:void(0)" class="text-primary">
                    Software
                  </a>
                </small>
                <h4 class="mt-2">
                  <a href="javascript:void(0)" class="text-dark title">
                    To Do List
                  </a>
                </h4>
                <p class="text-muted mt-2">
                  To Do List bisa mengatur kegiatan sehari-hari. Kegiatan atau tugas mu akan lebih mudah untuk dikelola dan dikerjakan, setelah menulis dalam To Do List.
                </p>
                <div class="pt-3 mt-3 border-top d-flex">
                  <img
                    src="https://png.pngtree.com/png-vector/20190727/ourmid/pngtree-business-work-to-do-list-png-image_1635099.jpg"
                    class="img-fluid avatar avatar-ex-sm rounded-pill mr-3 shadow"
                    alt=""
                  />
                  <div class="author mt-2">
                    <h6 class="mb-0">
                      <a href="/list" class="text-dark name">
                      Let's start!
                      </a>
                    </h6>
                  </div>
                </div>
              </div>
            </div>
          </div>

          
          
        </div>
      </div>
      <Footer />
    </div>
  );
}
