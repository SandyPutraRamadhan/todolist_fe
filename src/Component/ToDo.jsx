import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import axios from "axios";
import {
  MDBBtn,
  MDBCard,
  MDBCardBody,
  MDBCheckbox,
  MDBCol,
  MDBContainer,
  MDBIcon,
  MDBListGroup,
  MDBListGroupItem,
  MDBRow,
  MDBTooltip,
} from "mdb-react-ui-kit";
import Navbar from "./Navbar";
import Footer from "./Footer";

export default function App() {
  const [tugas, setTugas] = useState("");
  const [list, setList] = useState([]);
  const [task, setTask] = useState([]);
  const [totalPage, setTotalPage] = useState([]);

  const addList = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:3010/list", {
        tugas: tugas,
        userId: localStorage.getItem("userId"),
      });
      Swal.fire({
        icon: "success",
        title: "Kegiatan Di tambahkan",
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const getAllList = async () => {
    await axios
      .get(
        "http://localhost:3010/list?userId=" + localStorage.getItem("userId")
      )
      .then((response) => {
        setList(response.data.data);
      })
      .catch((error) => {
        console.log("Terjadi Kesalahan " + error);
      });
  };

  const deleteList = async (id) => {
    Swal.fire({
      title: "Apakah Yakin Ingin Delete?",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Tetap Delete!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:3010/list/" + id);
        Swal.fire("Berhasil Hapus!", "Kegiatan Anda Telah Di Hapus", "success");
      }
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    });
    // window.location.reload();
  };

  const selesai = async (data) => {
    Swal.fire({
      position: "center-center",
      icon: "success",
      title: "Task Selesai",
      showConfirmButton: false,
      timer: 1500,
    });
    await axios.post("http://localhost:3010/task/selesai", {
      toDoList: data.id,
      role: "SELESAI",
      userId: localStorage.getItem("userId"),
    });
  };
  useEffect(() => {
    getAllList();
  }, []);
  return (
    <div>
      <Navbar />
      <br />
      <MDBContainer className="py-5 bg-slate-500 rounded-[30px]">
        <MDBRow className="d-flex justify-content-center align-items-center h-100">
          <MDBCol>
            <MDBCard
              id="list1"
              style={{ borderRadius: ".75rem", backgroundColor: "#eff1f2" }}
            >
              <MDBCardBody className="py-4 px-4 px-md-5">
                <p className="h1 text-center mt-3 mb-4 pb-3 text-success">
                  <MDBIcon fas icon="check-square" className="me-1" />
                  <u>My Todo-List</u>
                </p>
                <div className="pb-2">
                  <form onSubmit={addList}>
                    <div className="d-flex flex-row align-items-center">
                      <input
                        type="text"
                        className="form-control form-control-lg "
                        placeholder="Tambahkan Kegiatan Yang Akan Kamu Kerjakan Hari Ini ..."
                        value={tugas}
                        onChange={(e) => setTugas(e.target.value)}
                        required
                      />

                      <div>
                        <button type="submit" className="btn btn-success">
                          ADD
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
                <hr className="my-4" />

                {list.map((lists) => (
                  <MDBListGroup horizontal className="rounded-0 bg-transparent">
                    <MDBListGroupItem className="px-3 py-1 d-flex align-items-center flex-grow-1 border-0 bg-transparent">
                      {" "}
                      <p className="lead fw-normal mb-0 text-green-700">
                        {lists.tugas}
                      </p>
                    </MDBListGroupItem>
                    <MDBListGroupItem className="ps-3 pe-0 py-1 rounded-0 border-0 bg-transparent">
                      <div className="d-flex flex-row justify-content-end mb-1">
                    

                        <button
                          onClick={() => selesai(lists)}
                          className="me-4 text-lime-500"
                        >
                          <i class="fa-solid fa-list-check"></i>
                        </button>

                        <a href={"/edit/" + lists.id}>
                          <MDBIcon fas icon="pencil-alt" className="me-4" />
                        </a>
                        <button onClick={() => deleteList(lists.id)}>
                          <MDBIcon fas icon="trash-alt" color="danger" />
                        </button>
                      </div>
                      <div className="text-end text-muted">
                        <MDBTooltip
                          tag="a"
                          wrapperProps={{ href: "#!" }}
                          title="Tanggal Buat"
                        >
                          <p className="small text-muted mb-0">
                            <MDBIcon fas icon="info-circle" className="me-2" />
                            {lists.tanggalDibuat}
                          </p>
                        </MDBTooltip>
                      </div>
                    </MDBListGroupItem>
                  </MDBListGroup>
                ))}
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
        </MDBRow>
      </MDBContainer>
      <br />
   
      <Footer />
    </div>
  );
}
