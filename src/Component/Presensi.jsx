import React, { useState, useEffect } from "react";
import Navbar from "./Navbar";
import axios from "axios";
import Swal from "sweetalert2";
import Footer from "./Footer";

export default function Presensi() {
  const [totalPage, setTotalPage] = useState([]);
  const [list, setList] = useState([]);

  const getAllList = async (page = 0) => {
    await axios
      .get(
        `http://localhost:3010/presensi?page=${page}&userId=` +
          localStorage.getItem("userId")
      )
      .then((response) => {
        const pages = [];
        for (let i = 0; i < response.data.data.totalPages; i++) {
          pages.push(i);
        }
        setTotalPage(pages);
        setList(
          response.data.data.content.map((data) => ({
            ...data,
          }))
        );
      })
      .catch((error) => {
        console.log("Terjadi Kesalahan " + error);
      });
  };

  const deleteList = async (id) => {
    Swal.fire({
      title: "Apakah Yakin Ingin Hapus?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Tetap Delete!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:3010/presensi/" + id);
        Swal.fire("Berhasil Hapus!", "Presensi Anda Telah Di Hapus", "success");
      }
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    });
    // window.location.reload();
  };
  const addMasuk = async (e) => {
    e.preventDefault();

    try {
      await axios.post("http://localhost:3010/presensi/absen-masuk", {
        role: "MASUK",
        userId: localStorage.getItem("userId"),
      });
      Swal.fire({
        icon: "success",
        title: "Absen Masuk Di tambahkan",
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const addPulang = async (e) => {
    e.preventDefault();

    try {
      await axios.post("http://localhost:3010/presensi/absen-pulang", {
        role: "PULANG",
        userId: localStorage.getItem("userId"),
      });
      Swal.fire({
        icon: "success",
        title: "Absen Pulang Di tambahkan",
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getAllList();
  }, []);
  return (
    <div>
      <Navbar />
      <br />
  
      <div class="grid grid-cols-2 gap-3 text-center p-6">
      <a onClick={addMasuk} title="Tailwind CSS Marketing Components - Announcements" class=" group relative block h-full bg-slate-300 before:absolute before:inset-0 before:rounded-lg before:border-2 before:border-dashed before:border-black" href="/presensi"><div class="rounded-lg border-2 border-black bg-zinc-400 transition group-hover:-translate-x-2 group-hover:-translate-y-2"><div class="p-6"><span aria-hidden="true" role="img" class="text-xl"><i class="fa-solid fa-clipboard"></i></span><p class="mt-4 text-lg font-medium">ABSEN MASUK</p></div></div></a>
        
      <a onClick={addPulang}title="Tailwind CSS Marketing Components - Announcements" class="group relative block h-full bg-slate-300 before:absolute before:inset-0 before:rounded-lg before:border-2 before:border-dashed before:border-black" href="/todolist"><div class="rounded-lg border-2 border-black bg-zinc-400 transition group-hover:-translate-x-2 group-hover:-translate-y-2"><div class="p-6"><span aria-hidden="true" role="img" class="text-xl"><i class="fa-solid fa-right-from-bracket"></i></span><p class="mt-4 text-lg font-medium">ABSEN PULANG</p></div></div></a>

</div>
      <div className="p-4 overflow-x-auto">
        <table class=" shadow w-full md:w-full divide-y-2 divide-gray-200 text-sm ">
          <thead>
            <tr className="bg-gray-400">
              <th class="whitespace-nowrap px-4 py-2 text-left font-medium text-gray-900 ">
                No
              </th>
              <th class="whitespace-nowrap px-4 py-2 text-left font-medium text-gray-900">
                Status
              </th>
              <th class="whitespace-nowrap px-4 py-2 text-left font-medium text-gray-900">
                Jam
              </th>
              <th class="whitespace-nowrap px-4 py-2 text-left font-medium text-gray-900">
                Tanggal
              </th>
              <th class="whitespace-nowrap px-4 py-2 text-left font-medium text-gray-900">
                Aksi
              </th>
            </tr>
          </thead>

          <tbody class="divide-y divide-gray-200">
            {list.map((lists, idx) => {
              return (
                <tr key={lists.id} class="odd:bg-gray-50">
                  <td className="whitespace-nowrap px-4 py-2 font-medium text-gray-900">
                    {idx + 1}
                  </td>
                  <td class="whitespace-nowrap px-4 py-2 font-medium text-gray-900">
                    {lists.role}
                  </td>
                  <td class="whitespace-nowrap px-4 py-2 text-gray-700">
                    {lists.JamMulai}
                  </td>
                  <td class="whitespace-nowrap px-4 py-2 text-gray-700">
                    {lists.tanggalBuat}
                  </td>
                  <td>
                    <button
                      onClick={() => deleteList(lists.id)}
                      className="btn btn-danger"
                    >
                      Hapus
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
      <ol class="flex justify-center gap-1 text-xs font-medium">
        <li>
          <a
            href="#"
            class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100"
          >
            <span class="sr-only">Prev Page</span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-3 w-3"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                clip-rule="evenodd"
              />
            </svg>
          </a>
        </li>

        {totalPage.map((e, i) => (
          <li>
            <span
              onClick={() => {
                getAllList(i);
              }}
              key={i}
              class="cursor-pointer block h-8 w-8 rounded border border-gray-100 text-center leading-8"
            >
              {i + 1}
            </span>
          </li>
        ))}
        <li>
          <a
            href="#"
            class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100"
          >
            <span class="sr-only">Next Page</span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-3 w-3"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clip-rule="evenodd"
              />
            </svg>
          </a>
        </li>
      </ol>
      <Footer />
    </div>
  );
}
