import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { useNavigate } from 'react-router-dom';

function BasicExample() {

  const navigate = useNavigate();

  const logout = () => {
    localStorage.clear();
    window.location.reload()
    navigate("/");
  };
  return (
    <Navbar bg="light" expand="lg">
      <Container>
      <Navbar.Brand href="/home">
            <img
              alt=""
              src="https://camo.githubusercontent.com/33e1354ddfbca41fd53e6f1c8baaa3fcadf0168f6472d97168d7e8f727c6f7bd/68747470733a2f2f692e696d6775722e636f6d2f785873574c4c472e706e67"
              width="30"
              height="30"
              className="d-inline-block align-top"
            />{' '}
            Presensi & To Do List
          </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Nav className=" flex ml-auto">
          <Nav.Link className='font-bold text-lg hover:bg-green-400 rounded-[50px] text-center' href='/akun'><i class="fa-solid fa-user"></i></Nav.Link>
          <Nav.Link className='font-bold text-lg hover:bg-green-400 rounded-[50px] text-center' href='/task'><i class="fa-solid fa-book"></i></Nav.Link>

            {localStorage.getItem("userId") !== null ? (
              <Nav.Link className='font-bold text-lg md:hover:bg-green-400 rounded-[50px] tracking-[.15em]' onClick={logout}>Log-out</Nav.Link>
            ) : (
              <Nav.Link className='font-bold text-lg md:hover:bg-green-400 rounded-[50px] tracking-[.15em]' href='/'>Login</Nav.Link>
            )}


          </Nav>
      </Container>
    </Navbar>
  );
}

export default BasicExample;