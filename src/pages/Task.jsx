import React, { useEffect, useState } from 'react'
import axios from 'axios';
import Navbar from '../Component/Navbar'
import Swal from 'sweetalert2';
import Footer from '../Component/Footer';

export default function Task() {
    const [ task, setTask ] = useState([]);
    const [ totalPage, setTotalPage] =  useState([]);

    const getAllList = async (page = 0) => {
        await axios
      .get(`http://localhost:3010/task?page=${page}&userId=${localStorage.getItem("userId")}`)
      .then((response) => {
        const pages = []
          for (let i = 0; i < response.data.data.totalPages; i++) {
            pages.push(i)
           }
           setTotalPage(pages)
        setTask(response.data.data.content.map((e, i) => ({
          ...e,
          no:i+1,
        })));
      })
    }
    const deleteList = async (id) => {
      Swal.fire({
          title: 'Apakah Yakin Ingin Delete?',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Tetap Delete!'
        }).then((result) => {
          if (result.isConfirmed) {
            axios.delete("http://localhost:3010/task/" + id)
            Swal.fire(
              'Berhasil Hapus!',
              'Kegiatan Anda Telah Di Hapus',
              'success'
              )
            }
            setTimeout(() => {
              window.location.reload();
            }, 1500)
          })
          // window.location.reload();
        };
        const deleteAll = async() => {
          const confirm=  await  Swal.fire({
              title: ' Ingin Hapus Semua?',
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Tetap Hapus!'
            })
            if (!confirm.isConfirmed) {
              return 
            }
              await axios.delete(`http://localhost:3010/task?userId=${localStorage.getItem("userId")}`)
    
              Swal.fire({
                icon:"success",
                title:"Berhasil", 
                showConfirmButton:false,
                timer:1500
              }
              );
              setTimeout(() => {
                window.location.reload();
              }, 1500)
          }
    useEffect(() => {
        getAllList();
      }, []);
  return (
    <div>
      <Navbar />
       <button onClick={deleteAll} className='btn btn-danger md:ml-[1300px] md:mt-20 md:p-10'>Hapus Semua</button>
      <div className='p-4 overflow-x-auto'>
  <table class=" shadow w-full md:w-full divide-y-2 divide-gray-200 text-sm ">
    <thead>
      <tr className='bg-gray-400'>
        <th
          class="whitespace-nowrap px-4 py-2 text-left font-medium text-gray-900 "
        >
          No
        </th>
        <th
          class="whitespace-nowrap px-4 py-2 text-left font-medium text-gray-900"
        >
          Task
        </th>
        <th
          class="whitespace-nowrap px-4 py-2 text-left font-medium text-gray-900"
        >
          Waktu Selesai
        </th>
        <th
          class="whitespace-nowrap px-4 py-2 text-left font-medium text-gray-900"
        >
          Status
        </th>
        <th
          class="whitespace-nowrap px-4 py-2 text-left font-medium text-gray-900"
        >
          Hapus
        </th>
      </tr>
    </thead>

    <tbody class="divide-y divide-gray-200">
      {task.map((lists, id) => {
       return (
          <tr key={lists.id} class="odd:bg-gray-50">
            <td className='whitespace-nowrap px-4 py-2 font-medium text-gray-900'>{id + 1}</td>
          <td class="whitespace-nowrap px-4 py-2 font-medium text-gray-900">
            {lists.toDoList?.tugas.toString()}
          </td>
          <td class="whitespace-nowrap px-4 py-2 text-gray-700">{lists.toDoList?.tanggalDibuat.toString()}</td>
          <td class="whitespace-nowrap px-4 py-2 text-gray-700">{lists.role}</td>
          <td>
            <button onClick={() => deleteList(lists.id)} className='btn btn-danger'>Hapus</button>
          </td>
        </tr>
       )
    })} 
    </tbody>
  </table>
</div>
<ol class="flex justify-center gap-1 text-xs font-medium">
        <li>
          <a
            href="#"
            class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100"
          >
            <span class="sr-only">Prev Page</span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-3 w-3"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                clip-rule="evenodd"
              />
            </svg>
          </a>
        </li>

  
        {
          totalPage.map((e, i) => (
            <li>
        <span
          onClick={() => {
              getAllList(i)
          }}
          key={i}
          class="cursor-pointer block h-8 w-8 rounded border border-gray-100 text-center leading-8"
        >
          {i + 1}
        </span>
      </li>
          ))
        }
        <li>
          <a
            href="#"
            class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100"
          >
            <span class="sr-only">Next Page</span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-3 w-3"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clip-rule="evenodd"
              />
            </svg>
          </a>
        </li>
      </ol>
      <Footer />
    </div>
  )
}
