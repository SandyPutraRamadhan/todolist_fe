import React, { useState, useEffect } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import axios from 'axios';
import Swal from 'sweetalert2';

export default function ToDoEdit() {
    const param = useParams();
    const [ tugas, setTugas ] = useState("");

    const navigate = useNavigate();

    const updateList = async (e) => {
        e.preventDefault();
        Swal.fire({
                title: 'apakah yakin di edit datanya?',
                showCancelButton: true,
                confirmButtonText: 'Edit',
      }).then((result) => {
        if (result.isConfirmed) {
         axios.put("http://localhost:3010/list/" + param.id , {
            tugas:tugas,
            userId: localStorage.getItem("userId")
         }
          ).then(() => {
            navigate("/list")
            Swal.fire('Berhasil Mengedit!', '', 'success')
          }).catch((error) => {
            console.log(error);
          })
        }
      })
      }
    
      useEffect(() => {
        axios.get("http://localhost:3010/list/" + param.id)
        .then((response) => {
          const newList = response.data.data;
         setTugas(newList.tugas)
        })
        .catch((error) => {
          alert("Terjadi Kesalahan "+ error);
        })
      }, []);
  return (
    <div>
<div class="mx-auto max-w-screen-xl px-4 py-16 sm:px-6 lg:px-8">
  <div class="mx-auto max-w-lg text-center">
    <h1 class="text-2xl font-bold sm:text-3xl">Silahkan Edit Kegiatan Anda!</h1>

  
  </div>

  <form onSubmit={updateList} action="" class="mx-auto mt-8 mb-0 max-w-md space-y-4">
    <div>

      <label><strong>Kegiatan</strong></label>

      <div class="relative">
        <input
          type="text"
          value={tugas}
          onChange={(e) => setTugas(e.target.value)}
          class="w-full rounded-lg border-gray-200 p-4 pr-12 text-sm shadow-sm"
          placeholder="Enter Kegiatan"
          required
        />

      </div>
    </div>

    <div class="flex items-center justify-between">
      <a className='btn btn-danger' href="/list">Kembali</a>

      <button
        type="submit"
        class="ml-3 inline-block rounded-lg bg-blue-500 px-5 py-3 text-sm font-medium text-white"
      >
        Edit
      </button>
    </div>
  </form>
</div>

    </div>
  )
}
