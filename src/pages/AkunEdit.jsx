import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import Swal from "sweetalert2";
import { FormControl } from "react-bootstrap";
import "../Style/Akun.css";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

export default function ToDoEdit() {
  const param = useParams();
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [telepon, setTelepon] = useState("");
  const [password, setPassword] = useState("");
  const [foto, setFoto] = useState("");

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const navigate = useNavigate();

  const updateUser = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    
    formData.append("nama", nama);
    formData.append("alamat", alamat);
    formData.append("telepon", telepon);
    Swal.fire({
      title: "Ingin Edit Profile Anda?",
      showCancelButton: true,
      confirmButtonText: "Ya",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .put("http://localhost:3010/users/" + param.id, formData, {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          })
          .then(() => {
            navigate("/akun");
            Swal.fire("Berhasil Mengedit!", "", "success");
          })
          .catch((error) => {
            console.log(error);
          });
      }
    });
  };
  const updatePw = async (e) => {
    console.log("test");
    e.preventDefault();
    Swal.fire({
      title: "Ingin Update Password?",
      showCancelButton: true,
      confirmButtonText: "Ya",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .put(
            "http://localhost:3010/users/editpw/" +
              localStorage.getItem("userId"), {
                password: password
              })
          .then(() => {
            navigate("/akun")
            Swal.fire("Berhasil Mengedit!", "", "success");
          })
          .catch((error) => {
            console.log(error);
          });
      }
    });
  };
 

  useEffect(() => {
    axios
      .get("http://localhost:3010/users/" + param.id)
      .then((response) => {
        const newUser = response.data.data;
        setNama(newUser.nama);
        setAlamat(newUser.alamat);
        setTelepon(newUser.telepon);
        setFoto(newUser.foto);
        setPassword(newUser.password)
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, []);
  return (
    <div>
      <div class="mx-auto max-w-screen-xl px-4 py-16 sm:px-6 lg:px-8 shadow-md">
        <div class="mx-auto max-w-lg text-center">
          <h1 class="text-2xl font-bold sm:text-3xl">
            Ubah Profile Anda
          </h1>
        </div>

        <form
          onSubmit={updateUser}
          className="mx-auto mt-8 mb-0 max-w-md space-y-4"
        >
          <div>
            <div class="relative">
              <input
                value={nama}
                onChange={(e) => setNama(e.target.value)}
                class="w-full rounded-lg border-gray-200 p-4 pr-12 text-sm shadow-sm"
                required
              />
            </div>
          </div>

          <div>
            <div class="relative">
              <input
                value={alamat}
                onChange={(e) => setAlamat(e.target.value)}
                class="w-full rounded-lg border-gray-200 p-4 pr-12 text-sm shadow-sm"
                required
              />
            </div>
          </div>

          <div>
            <div class="relative">
              <input
                value={telepon}
                onChange={(e) => setTelepon(e.target.value)}
                class="w-full rounded-lg border-gray-200 p-4 pr-12 text-sm shadow-sm"
                required
              />
            </div>
          </div>
          
      <Button variant="primary" onClick={handleShow}>
        Ganti Password
      </Button>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Ingin Ubah Password Anda?</Modal.Title>
        </Modal.Header>
        <Modal.Body><div>
            <div class="relative">
              <input
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                class="w-full rounded-lg border-gray-200 p-4 pr-12 text-sm shadow-sm"
                required
              />
            </div>
          </div>
          </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Tutup
          </Button>
          <Button variant="primary" onClick={updatePw}>
            Simpan
          </Button>
        </Modal.Footer>
      </Modal>

          <div class="flex items-center justify-between">
            <a className="btn btn-danger" href="/akun">
              Kembali
            </a>

            <button type="submit" className="btn btn-success">
              Edit
            </button>

           
          </div>
        </form>
      </div>
    </div>
  );
}
