import { Route, Routes } from 'react-router-dom';
import Login from './Component/Login';
import Home from './Component/Home';
import Register from './Component/Register';
import List from './Component/ToDo.jsx'
import Presensi from './Component/Presensi';
import ToDoEdit from './pages/ToDoEdit';
import Akun from './pages/Akun';
import AkunEdit from './pages/AkunEdit'
import Task from './pages/Task';

function App() {
  return (
    <div>
      <Routes>
        <Route path='/' element={<Login/>}/>
        <Route path='/register' element={<Register/>}/>
        <Route path='/home' element={<Home/>} />
        <Route path='/list' element={<List/>} />
        <Route path='/presensi' element={<Presensi/>} />
        <Route path='/akun' element={<Akun/>} />
        <Route path='/task' element={<Task/>} />
        <Route path='/edit/:id' element={<ToDoEdit/>} />
        <Route path='/editAkun/:id' element={<AkunEdit/>} />
      </Routes>
    </div>
  );
}

export default App;
